package main

import (
	"fmt"
	"path/filepath"

	"gitlab.com/chloris_/autoplot/internal/autoplot"
)

// VERSION represents the version of the program
const VERSION string = "1.0.0"

// DATE represents the date of release of the current program version
const DATE string = "2020-11-05"

// Path to the directory that is searched for plot scripts and in which
// subdirectories for plots are created
const path string = "."

func main() {
	// Display greeting
	fmt.Printf("autoplot version %s (%s)\n\n", VERSION, DATE)

	// Get absolute path of the directory for informational purposes
	absolute, err := filepath.Abs(path)
	if err != nil {
		fmt.Printf("Error while getting absolute path of '%s': %s\n", path, err)
		absolute = path
	}

	// Autoplot scripts in the directory
	fmt.Printf("Searching for plot scripts in directory '%s'\n", absolute)
	autoplot := autoplot.NewAutoplot(path)
	n, err := autoplot.Autoplot()
	if err != nil {
		fmt.Printf("Autoplot error: %v\n", err)
		return
	}
	fmt.Printf("Plotted %v scripts\n", n)

	// Convert plots if any of them require it
	fmt.Println("\nConverting plots")
	n, err = autoplot.Convert()
	if err != nil {
		fmt.Printf("Conversion error: %v\n", err)
		return
	}
	fmt.Printf("Converted %v plots\n", n)
}
