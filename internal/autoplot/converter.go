package autoplot

import (
	"fmt"
	"os/exec"
	"path/filepath"
	"strings"
)

// Convert converts files that were plotted by Autoplot method of this
// Autoplot instance. Only files of certain types are converted. Converted
// files are placed in an appropriate subdirectory based on their format.
// Returns number of successfully converted plots.
//
// Currently implemented conversions: SVG to PDF
func (ap *Autoplot) Convert() (int, error) {
	n := 0
	var err error

	for _, plot := range ap.plots {
		switch plot.Filetype {
		case "svg":
			err = ap.convertSVGToPDF(plot)
		}

		if err == nil {
			n++
		}
	}

	return n, nil
}

// convertSVGToPDF converts a SVG plot to PDF using rsvg-convert.
func (ap *Autoplot) convertSVGToPDF(plot Plot) error {
	// Create subdir for the converted file
	subdir, err := ap.createSubdir("pdf")
	if err != nil {
		fmt.Printf("Failed to create subdir 'pdf': %v\n", err)
		return err
	}

	// Figure out path of the converted file
	base := filepath.Base(plot.Path)
	ext := filepath.Ext(base)
	name := strings.TrimSuffix(base, ext)
	converted := name + ".pdf"
	convertedPath := filepath.Join(subdir, converted)

	// Perform conversion
	cmd := exec.Command("rsvg-convert", "-f", "pdf", plot.Path, "-o", convertedPath)
	output, err := cmd.CombinedOutput()
	// Print potential rsvg-convert output
	fmt.Printf("%s", output)
	if err != nil {
		fmt.Printf("Conversion of '%v' to PDF failed\n", plot.Path)
		return err
	}

	// Print the name of successfully converted file
	fmt.Printf("- %v\n", convertedPath)

	return nil
}
