package autoplot

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

// Precompiled regexp for extraction of output file from Gnuplot script
var regexpOutput = regexp.MustCompile(`^\s*set\s+output\s+"(.+)"\s*$`)

// scriptInfo contains information, extracted from a Gnuplot script, used
// for plotting and treatment of the resulting plot.
type scriptInfo struct {
	// Path to the plot script the data was extracted from
	scriptPath string
	// Output path of the plot script
	output string
	// Format of the plot file extracted from extension of the output field.
	// It should always be in lower case.
	outputType string
}

// getScriptInfo reads provided Gnuplot script and returns data in form of
// a scriptInfo struct. Output type is always returned in lower case.
func getScriptInfo(path string) (scriptInfo, error) {
	si := scriptInfo{}

	// Open the script file
	file, err := os.Open(path)
	if err != nil {
		return si, err
	}
	defer file.Close()

	// Read the file line by line and look for required data
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		match := regexpOutput.FindStringSubmatch(scanner.Text())
		if match != nil {
			si.scriptPath = path
			si.output = match[1]
			// Extract output type from output file path
			ext := filepath.Ext(si.output)
			// Ext returns extensions including ".". Extension is considered
			// valid if it contains more than just a dot.
			if len(ext) > 1 {
				// Cut off the dot and make sure file type is in lower case
				// only to prevent errors further down the road
				si.outputType = strings.ToLower(ext[1:])
			} else {
				fmt.Printf("Cannot determine output type from '%v'\n", ext)
				return si, errors.New("invalid output type")
			}

			return si, nil
		}
	}
	if err = scanner.Err(); err != nil {
		return si, err
	}

	// If execution reaches this point, required data was not found in the
	// script and no error occured. The script should be skipped.
	fmt.Printf("No required data found in script '%v'\n", path)
	return si, errors.New("required data not found")
}
