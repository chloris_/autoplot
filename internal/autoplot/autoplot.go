package autoplot

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
)

// gnuplotExtensions holds possible extensions for Gnuplot scripts
var gnuplotExtensions = [...]string{".gnuplot", ".gpi", ".plt"}

// Autoplot plots all Gnuplot scripts in a given directory and keeps track of
// files plotted. It should be constructed with function NewAutoplot.
type Autoplot struct {
	// Path to the directory that is searched for plot scripts
	path string
	// List of files that were successfully plotted
	plots []Plot
}

// NewAutoplot constructs a new Autoplot object. It requires a path to the
// directory that is to be scanned for Gnuplot plot scripts. If the path
// provided is an empty string, current working directory is used.
func NewAutoplot(path string) Autoplot {
	if path == "" {
		path = "."
	}
	return Autoplot{
		path,
		make([]Plot, 0, 10),
	}
}

// Autoplot searches given directory for Gnuplot plot scripts and plots them
// in subfolders by their file type. If an empty string is provided,
// current working directory is used as source path. Returns number of files
// successfully plotted.
func (ap *Autoplot) Autoplot() (int, error) {
	// List directory and iterate entries
	files, err := ioutil.ReadDir(ap.path)
	if err != nil {
		return 0, err
	}
	for _, file := range files {
		if file.IsDir() {
			continue
		}

		// If file seems to be a Gnuplot script, plot it
		name := file.Name()
		if isGnuplotScript(name) {
			err = ap.plot(name)
			if err != nil {
				fmt.Printf("Plotting '%v' failed\n", name)
			}
		} else {
			fmt.Printf("'%v' is not a Gnuplot script, skipping\n", name)
		}
	}

	return len(ap.plots), nil
}

// Plots given Gnuplot scripts and moves the resulting file to a subfolder
// of ap.path with name of the format of the plot.
func (ap *Autoplot) plot(path string) error {
	// Get relevant plot information from the script
	si, err := getScriptInfo(path)
	if err != nil {
		fmt.Printf("Failed to get script info, skipping script '%v'\n", path)
		return err
	}

	// Create a subdirectory for the plot to be moved into based on plot
	// output format
	subdir, err := ap.createSubdir(si.outputType)
	if err != nil {
		return err
	}

	// Execute Gnuplot
	cmd := exec.Command("gnuplot", path)
	output, err := cmd.CombinedOutput()
	// Print potential Gnuplot output
	fmt.Printf("%s", output)
	if err != nil {
		fmt.Printf("Error while running gnuplot on script '%v': %v\n", path, err)
		return err
	}

	// Check if the plot exists, generate new path for it and move it
	_, err = os.Stat(si.output)
	if err != nil {
		fmt.Printf("Cannot find plot '%v'", si.output)
		return err
	}
	movePath := filepath.Join(subdir, si.output)
	err = os.Rename(si.output, movePath)
	if err != nil {
		fmt.Printf("Error while moving file '%v'\n", si.output)
		return err
	}

	// Add moved file to the list of plots
	plot := Plot{
		Path:     movePath,
		Filetype: si.outputType,
	}
	ap.plots = append(ap.plots, plot)

	// Print the name of successfully plotted and moved file
	fmt.Printf("- %v\n", movePath)

	return nil
}

// isGnuplotScript checks a file name against gnuplotExtensions array and
// determines by extension whether it is a Gnuplot script.
func isGnuplotScript(name string) bool {
	ext := filepath.Ext(name)

	if ext == "" {
		return false
	}

	for _, e := range gnuplotExtensions {
		if ext == e {
			return true
		}
	}

	return false
}

// createSubdir creates a subdirectory with given name in ap.path, if it does
// not yet exist. Returns path to the subdirectory.
func (ap *Autoplot) createSubdir(subdir string) (string, error) {
	subdirPath := filepath.Join(ap.path, subdir)
	file, err := os.Stat(subdirPath)

	// If the error was raised because subdirectory does not yet exist,
	// create it
	if os.IsNotExist(err) {
		err = os.Mkdir(subdirPath, os.ModePerm)
		if err != nil {
			fmt.Printf("Error while creating subdir '%v': %v\n", subdirPath, err)
			return "", err
		}
		return subdirPath, nil
	}

	// If the error is of another type, report it
	if err != nil {
		fmt.Printf("Error while creating subdir '%v': %v\n", subdirPath, err)
		return "", err
	}

	// If there is a file with name that should be subdir, raise an error
	if file.IsDir() == false {
		fmt.Printf("'%v' is a regular file, cannot create subdirectory!\n", subdirPath)
		return "", fmt.Errorf("'%v' is not a directory", subdirPath)
	}
	// Subdir already exists and is not a file
	return subdirPath, nil
}

// GetPlots returns a copy of the collection of plotted files.
func (ap *Autoplot) GetPlots() []Plot {
	tmp := make([]Plot, len(ap.plots))
	copy(tmp, ap.plots)
	return tmp
}
