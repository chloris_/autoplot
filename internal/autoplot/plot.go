package autoplot

// Plot contains information about a file plotted with a Gnuplot script:
// path to the plotted file and its type. This information can be used to
// convert plots to another format if necessary.
type Plot struct {
	Path     string
	Filetype string
}
