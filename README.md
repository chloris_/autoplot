# autoplot

Automatically plots and converts all Gnuplot plots in the directory. Files are
neatly sorted in subdirectories based on their type.


## How to compile

Simply use the `go` compiler:
```bash
go build -o autoplot cmd/autoplot/main.go
```
This will generate an executable named `autoplot`.


## How to use

Invoke `autoplot` without any command line arguments in the directory with
Gnuplot scripts you wish to plot and possibly convert.

`autoplot` does not check timestamps of the plots. Gnuplot scripts are
processed each time the program is run. Format of plotted files is determined
from the extension of the path obtained from `set output` statement in the
Gnuplot script.

The following extensions are recognized as Gnuplot scripts:
- `.gnuplot`
- `.gpi`
- `.plt`

The following conversions are currently supported:
- SVG to PDF
